<?php
if (!defined('FLUX_ROOT')) exit;

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "https://hahiu.com" || $http_origin == "http://localhost") {
    header('content-type:text/html;charset=utf-8');
    header("Access-Control-Allow-Origin: $http_origin");
}

$title = 'Viewing Item';

require_once 'Flux/TemporaryTable.php';

if($server->isRenewal) {
	$fromTables = array("{$server->charMapDatabase}.item_db_re", "{$server->charMapDatabase}.item_db2_re");
	$fromMobTables = array("{$server->charMapDatabase}.mob_db_re", "{$server->charMapDatabase}.mob_db2_re");
} else {
	$fromTables = array("{$server->charMapDatabase}.item_db", "{$server->charMapDatabase}.item_db2");
	$fromMobTables = array("{$server->charMapDatabase}.mob_db", "{$server->charMapDatabase}.mob_db2");
}
$tableName = "{$server->charMapDatabase}.items";
$tempTable = new Flux_TemporaryTable($server->connection, $tableName, $fromTables);
$shopTable = Flux::config('FluxTables.ItemShopTable');
$itemDescTable = Flux::config('FluxTables.ItemDescTable');

$itemID = $params->get('id');

$col  = 'items.id AS item_id, name_english AS identifier, ';
$col .= 'name_japanese AS name, type, ';
$col .= 'price_buy, price_sell, weight/10 AS weight, defence, `range`, slots, ';
$col .= 'equip_jobs, equip_upper, equip_genders, equip_locations, ';
$col .= 'weapon_level, equip_level AS equip_level_min, refineable, view, script, ';
$col .= 'equip_script, unequip_script, origin_table, ';
$col .= "$shopTable.cost, $shopTable.id AS shop_item_id, ";
if(Flux::config('ShowItemDesc')){
    $col .= 'itemdesc, ';
}
$col .= $server->isRenewal ? '`atk:matk` AS attack' : 'attack';

$sql  = "SELECT $col FROM {$server->charMapDatabase}.items ";
$sql .= "LEFT OUTER JOIN {$server->charMapDatabase}.$shopTable ON $shopTable.nameid = items.id ";
if(Flux::config('ShowItemDesc')){
    $sql .= "LEFT OUTER JOIN {$server->charMapDatabase}.$itemDescTable ON $itemDescTable.itemid = items.id ";
}
$sql .= "WHERE items.id = ? LIMIT 1";

$sth  = $server->connection->getStatement($sql);
$sth->execute(array($itemID));

$item = $sth->fetch();
$isCustom = null;

if ($item) {
	$title = "Viewing Item ($item->name)";
	$isCustom = (bool)preg_match('/item_db2$/', $item->origin_table);

	if($server->isRenewal) {
		$item = $this->itemFieldExplode($item, 'attack', ':', array('attack','matk'));
		$item = $this->itemFieldExplode($item, 'equip_level_min', ':', array('equip_level_min','equip_level_max'));
	}

	$mobDB      = "{$server->charMapDatabase}.monsters";
	if($server->isRenewal) {
		$fromTables = array("{$server->charMapDatabase}.mob_db_re", "{$server->charMapDatabase}.mob_db2_re");
	} else {
		$fromTables = array("{$server->charMapDatabase}.mob_db", "{$server->charMapDatabase}.mob_db2");
	}
	$mobTable   = new Flux_TemporaryTable($server->connection, $mobDB, $fromTables);

	$col  = 'ID AS monster_id, iName AS monster_name, LV AS monster_level, ';
	$col .= 'Race AS monster_race, (Element%10) AS monster_element, (Element/20) AS monster_ele_lv, MEXP AS mvp_exp, ';

	// Normal drops.
	$col .= 'Drop1id AS drop1_id, Drop1per AS drop1_chance, ';
	$col .= 'Drop2id AS drop2_id, Drop2per AS drop2_chance, ';
	$col .= 'Drop3id AS drop3_id, Drop3per AS drop3_chance, ';
	$col .= 'Drop4id AS drop4_id, Drop4per AS drop4_chance, ';
	$col .= 'Drop5id AS drop5_id, Drop5per AS drop5_chance, ';
	$col .= 'Drop6id AS drop6_id, Drop6per AS drop6_chance, ';
	$col .= 'Drop7id AS drop7_id, Drop7per AS drop7_chance, ';
	$col .= 'Drop8id AS drop8_id, Drop8per AS drop8_chance, ';
	$col .= 'Drop9id AS drop9_id, Drop9per AS drop9_chance, ';

	// Card drops.
	$col .= 'DropCardid AS dropcard_id, DropCardper AS dropcard_chance, ';

	// Mode
	$col .= 'Mode as mode, ';

	// MVP rewards.
	$col .= 'MVP1id AS mvpdrop1_id, MVP1per AS mvpdrop1_chance, ';
	$col .= 'MVP2id AS mvpdrop2_id, MVP2per AS mvpdrop2_chance, ';
	$col .= 'MVP3id AS mvpdrop3_id, MVP3per AS mvpdrop3_chance';

	$sql  = "SELECT $col FROM $mobDB WHERE ";

	// Normal drops.
	$sql .= 'Drop1id = ? OR ';
	$sql .= 'Drop2id = ? OR ';
	$sql .= 'Drop3id = ? OR ';
	$sql .= 'Drop4id = ? OR ';
	$sql .= 'Drop5id = ? OR ';
	$sql .= 'Drop6id = ? OR ';
	$sql .= 'Drop7id = ? OR ';
	$sql .= 'Drop8id = ? OR ';
	$sql .= 'Drop9id = ? OR ';

	// Card drops.
	$sql .= 'DropCardid = ? OR ';

	// MVP rewards.
	$sql .= 'MVP1id = ? OR ';
	$sql .= 'MVP2id = ? OR ';
	$sql .= 'MVP3id = ? ';

	$sth  = $server->connection->getStatement($sql);

	$res = $sth->execute(array_fill(0, 13, $itemID));

	$dropResults = $sth->fetchAll();

	$itemDrops   = array();
	$dropNames   = array(
		'drop1', 'drop2', 'drop3', 'drop4', 'drop5', 'drop6', 'drop7', 'drop8', 'drop9',
		'dropcard', 'mvpdrop1', 'mvpdrop2', 'mvpdrop3'
	);

	// Sort callback.
	function __tmpSortDrops($arr1, $arr2)
	{
		if ($arr1['drop_chance'] == $arr2['drop_chance']) {
			return strcmp($arr1['monster_name'], $arr2['monster_name']);
		}

		return $arr1['drop_chance'] < $arr2['drop_chance'] ? 1 : -1;
	}

	$MD_MVP 				= 0x0080000;
	$MD_KNOCKBACK_IMMUNE 	= 0x0200000;
	$MD_DETECTOR 			= 0x2000000;
	$MD_STATUS_IMMUNE 		= 0x4000000;

	$isBossMode = $MD_KNOCKBACK_IMMUNE | $MD_DETECTOR | $MD_STATUS_IMMUNE;

	// Hard Code for Bio Lab Drops
	$itemFixedRate = array(
		6814,
		6815,
		6816,
		6817,
		6818,
		6819,
		22687,
		23016,
		22679,
		25127,
		25128,
		25129,
		25130,
		25131
	);
	
	foreach ($dropResults as $drop) {
		foreach ($dropNames as $dropName) {
			$dropID     = $drop->{$dropName.'_id'};
			$dropChance = $drop->{$dropName.'_chance'};

			$mode = $drop->mode;
			$mode_string = "";

			if (($mode & $MD_MVP) == $MD_MVP) {
				$mode_string = "MVP";
			} else if (($mode & $isBossMode) == $isBossMode) {
				$mode_string = "Boss";
			}

			if ($dropID == $itemID) {
				$dropArray = array(
					'monster_id'      => $drop->monster_id,
					'monster_name'    => $drop->monster_name,
					'monster_level'   => $drop->monster_level,
					'monster_race'    => $drop->monster_race,
					'monster_element' => $drop->monster_element,
					'monster_ele_lv'  => $drop->monster_ele_lv,
					'drop_id'         => $itemID,
					'drop_chance'     => $dropChance
				);

				if (in_array($itemID, $itemFixedRate)) {
					$adjust = $server->dropRates['Fixed'];
					$dropArray['type'] = 'fixed';
				}
				elseif (preg_match('/^dropcard/', $dropName)) {
					$adjust = $server->dropRates['Card' . $mode_string];
					$dropArray['type'] = 'card';
				}
				elseif (preg_match('/^mvp/', $dropName)) {
					$adjust = $server->dropRates['MvpItem'];
					$dropArray['type'] = 'mvp';
				}
				elseif (preg_match('/^drop/', $dropName)) {
					switch($item->type) {
						case 0: // Healing
							$adjust = $server->dropRates['Heal' . $mode_string];
							break;

						case 2: // Useable
						case 18: // Cash Useable
							$adjust = $server->dropRates['Useable' . $mode_string];
							break;

						case 4: // Weapon
						case 5: // Armor
						case 8: // Pet Armor
							$adjust = $server->dropRates['Equip' . $mode_string];
							break;

						default: // Common
							$adjust = $server->dropRates['Common' . $mode_string];
							break;
					}

					$dropArray['type'] = 'normal';
				}

				$dropArray['drop_chance'] = $dropArray['drop_chance'] * $adjust / 10000;

				if ($dropArray['drop_chance'] > 100) {
					$dropArray['drop_chance'] = 100;
				}

				$itemDrops[] = $dropArray;
			}
		}
	}

	// Sort so that monsters are ordered by drop chance and name.
	usort($itemDrops, '__tmpSortDrops');

    /**
     * Custom API
     */
    $icon = $this->iconImage($item->item_id);
    $image = $this->itemImage($item->item_id);

    $locs = $this->equipLocations($item->equip_locations);
    $upper = $this->equipUpper($item->equip_upper);
    $jobs = $this->equippableJobs($item->equip_jobs);

    $g = $item->equip_genders;
    if ($g === '0') {
        $gender = 'Nam';
    }
    else if ($g === '1') {
        $gender = 'Nữ';
    }
    else if ($g === '2') {
        $gender = 'Cả Nam và Nữ';
    }
    else {
        $gender = 'Không';
    }

    $itemInfo = array (
        'itemInfo' => array(
            'item_id' => $item->item_id,
            'name'  => $item->name,
            'icon'  => ($icon) ? $icon : '',
            'image' => ($image) ? $image : '',
            'identifier' => $item->identifier,
            'type' => $this->itemTypeText($item->type, $item->view),
            'npc_buy' => number_format((int)$item->price_buy),
            'npc_sell'=> (is_null($item->price_sell) && $item->price_buy) ? number_format(floor($item->price_buy / 2)) : number_format((int)$item->price_sell),
            'weight'  => round($item->weight, 1),
            'weapon_level' => number_format((int)$item->weapon_level),
            'range' => number_format((int)$item->range),
            'defense' => number_format((int)$item->defence),
            'slots' => number_format((int)$item->slots),
            'refineable' => ($item->refineable) ? 1 : 0,
            'min_equip_level' => number_format((int)$item->equip_level_min),
            'attack' => number_format((int)$item->attack),
            'matk' => number_format((int)$item->matk),
            'locs'  => ($locs) ? implode(' + ', $locs) : '',
            'equip_upper' => ($upper) ? implode(' / ', $upper) : '',
            'equip_jobs' => ($jobs) ? implode(' / ', $jobs) : '',
            'equip_gender' => $gender,
            'itemdesc' => ($item->itemdesc) ? htmlspecialchars($item->itemdesc) : ''
        ),
        'itemDrops' => $itemDrops
    );
    echo json_encode($itemInfo, JSON_UNESCAPED_UNICODE);
    die;
}
?>
