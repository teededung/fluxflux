<?php
if (!defined('FLUX_ROOT')) exit;

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "https://hahiu.com" || $http_origin == "http://localhost") {
	header('content-type:text/html;charset=utf-8');
	header("Access-Control-Allow-Origin: $http_origin");
}

$title = 'Viewing Monster';
$mobID = $params->get('id');

require_once 'Flux/TemporaryTable.php';

// Monsters table.
$mobDB      = "{$server->charMapDatabase}.monsters";
//here needs the same check if the server is renewal or not, I'm just lazy to do it by myself
if($server->isRenewal) {
	$fromTables = array("{$server->charMapDatabase}.mob_db_re", "{$server->charMapDatabase}.mob_db2_re");
} else {
	$fromTables = array("{$server->charMapDatabase}.mob_db", "{$server->charMapDatabase}.mob_db2");
}
$tempMobs   = new Flux_TemporaryTable($server->connection, $mobDB, $fromTables);

// Monster Skills table.
$skillDB    = "{$server->charMapDatabase}.mobskills";
if($server->isRenewal) {
	$fromTables = array("{$server->charMapDatabase}.mob_skill_db_re", "{$server->charMapDatabase}.mob_skill_db2_re");
} else {
	$fromTables = array("{$server->charMapDatabase}.mob_skill_db", "{$server->charMapDatabase}.mob_skill_db2");
}

$tempSkills = new Flux_TemporaryTable($server->connection, $skillDB, $fromTables);

// Items table.
if($server->isRenewal) {
	$fromTables = array("{$server->charMapDatabase}.item_db_re", "{$server->charMapDatabase}.item_db2_re");
} else {
	$fromTables = array("{$server->charMapDatabase}.item_db", "{$server->charMapDatabase}.item_db2");
}
$itemDB    = "{$server->charMapDatabase}.items";
$tempItems = new Flux_TemporaryTable($server->connection, $itemDB, $fromTables);

$col  = 'origin_table, ID as monster_id, Sprite AS sprite, kName AS kro_name, iName AS iro_name, LV AS level, HP AS hp, ';
$col .= 'EXP AS base_exp, JEXP as job_exp, Range1 AS range1, Range2 AS range2, Range3 AS range3, ';
$col .= 'DEF AS defense, MDEF AS magic_defense, ATK1 AS attack1, ATK2 AS attack2, DEF AS defense, MDEF AS magic_defense, ';
$col .= 'STR AS strength, AGI AS agility, VIT AS vitality, `INT` AS intelligence, DEX AS dexterity, LUK AS luck, ';
$col .= 'Scale AS size, Race AS race, (Element%10) AS element_type, (Element/20) AS element_level, Mode AS mode, ';
$col .= 'Speed AS speed, aDelay AS attack_delay, aMotion AS attack_motion, dMotion AS delay_motion, ';
$col .= 'MEXP AS mvp_exp, ';

// Item drops.
$col .= 'Drop1id AS drop1_id, Drop1per AS drop1_chance, ';
$col .= 'Drop2id AS drop2_id, Drop2per AS drop2_chance, ';
$col .= 'Drop3id AS drop3_id, Drop3per AS drop3_chance, ';
$col .= 'Drop4id AS drop4_id, Drop4per AS drop4_chance, ';
$col .= 'Drop5id AS drop5_id, Drop5per AS drop5_chance, ';
$col .= 'Drop6id AS drop6_id, Drop6per AS drop6_chance, ';
$col .= 'Drop7id AS drop7_id, Drop7per AS drop7_chance, ';
$col .= 'Drop8id AS drop8_id, Drop8per AS drop8_chance, ';
$col .= 'Drop9id AS drop9_id, Drop9per AS drop9_chance, ';
$col .= 'DropCardid AS dropcard_id, DropCardper AS dropcard_chance, ';

// MVP drops.
$col .= 'MVP1id AS mvpdrop1_id, MVP1per AS mvpdrop1_chance, ';
$col .= 'MVP2id AS mvpdrop2_id, MVP2per AS mvpdrop2_chance, ';
$col .= 'MVP3id AS mvpdrop3_id, MVP3per AS mvpdrop3_chance ';

$sql  = "SELECT $col FROM $mobDB WHERE ID = ? LIMIT 1";
$sth  = $server->connection->getStatement($sql);
$sth->execute(array($mobID));
$monster = $sth->fetch();

function get_monster_type($monster) {
	$MD_MVP 				= 0x0080000;
	$MD_KNOCKBACK_IMMUNE 	= 0x0200000;
	$MD_DETECTOR 			= 0x2000000;
	$MD_STATUS_IMMUNE 		= 0x4000000;

	$isBossMode = $MD_KNOCKBACK_IMMUNE | $MD_DETECTOR | $MD_STATUS_IMMUNE;

	$mode = $monster->mode;
	$string = "";

	if (($mode & $MD_MVP) == $MD_MVP) {
		$string = "MVP";
	} else if (($mode & $isBossMode) == $isBossMode) {
		$string = "Boss";
	}
	return $string;
}

if ($monster) {
	$title   = "Viewing Monster ({$monster->kro_name})";

	$monster->boss = $monster->mvp_exp;

	$monster->base_exp = $monster->base_exp * $server->expRates['Base'] / 100;
	$monster->job_exp  = $monster->job_exp * $server->expRates['Job'] / 100;
	$monster->mvp_exp  = $monster->mvp_exp * $server->expRates['Mvp'] / 100;

	$dropIDs = array(
		'drop1'    => $monster->drop1_id,
		'drop2'    => $monster->drop2_id,
		'drop3'    => $monster->drop3_id,
		'drop4'    => $monster->drop4_id,
		'drop5'    => $monster->drop5_id,
		'drop6'    => $monster->drop6_id,
		'drop7'    => $monster->drop7_id,
		'drop8'    => $monster->drop8_id,
		'drop9'    => $monster->drop9_id,
		'dropcard' => $monster->dropcard_id,
		'mvpdrop1' => $monster->mvpdrop1_id,
		'mvpdrop2' => $monster->mvpdrop2_id,
		'mvpdrop3' => $monster->mvpdrop3_id
	);

	$sql = "SELECT id, name_japanese, type FROM $itemDB WHERE id IN (".implode(', ', array_fill(0, count($dropIDs), '?')).")";
	$sth = $server->connection->getStatement($sql);
	$sth->execute(array_values($dropIDs));
	$items = $sth->fetchAll();

	$needToSet = array();
	if ($items) {
		foreach ($dropIDs AS $dropField => $dropID) {
			$needToSet[$dropField] = true;
		}

		foreach ($items as $item) {
			foreach ($dropIDs AS $dropField => $dropID) {
				if ($needToSet[$dropField] && $dropID == $item->id) {
					$needToSet[$dropField] = false;
					$monster->{$dropField.'_name'} = $item->name_japanese;
					$monster->{$dropField.'_type'} = $item->type;
				}
			}
		}
	}

	// Hard Code for Bio Lab Drops
	$itemFixedRate = array(
		6814,
		6815,
		6816,
		6817,
		6818,
		6819,
		22687,
		23016,
		22679,
		25127,
		25128,
		25129,
		25130,
		25131
	);

	$itemDrops = array();
	foreach ($needToSet as $dropField => $isset) {
		if ($isset === false) {
			$itemDrops[$dropField] = array(
				'id'     => $monster->{$dropField.'_id'},
				'name'   => $monster->{$dropField.'_name'},
				'chance' => $monster->{$dropField.'_chance'}
			);

			$monster_type = get_monster_type($monster);

			if (in_array($itemDrops[$dropField]['id'], $itemFixedRate)) {
				$adjust = $server->dropRates['Fixed'];
				$itemDrops[$dropField]['type'] = 'fixed';
			}
			else if (preg_match('/^dropcard/', $dropField)) {
				$adjust = $server->dropRates['Card' . $monster_type];
				$itemDrops[$dropField]['type'] = 'card';
			}
			elseif (preg_match('/^mvpdrop/', $dropField)) {
				$adjust = $server->dropRates['MvpItem'];
				$itemDrops[$dropField]['type'] = 'mvp';
			}
			elseif (preg_match('/^drop/', $dropField)) {
				switch($monster->{$dropField.'_type'}) {
					case 0: // Healing
						$adjust = $server->dropRates['Heal' . $monster_type];
						break;

					case 2: // Usable
					case 18: // Cash Usable
						$adjust = $server->dropRates['Useable' . $monster_type];
						break;

					case 4: // Weapon
					case 5: // Armor
					case 8: // Pet Armor
						$adjust = $server->dropRates['Equip' . $monster_type];
						break;

					default: // Common
						$adjust = $server->dropRates['Common' . $monster_type];
						break;
				}
				$itemDrops[$dropField]['type'] = 'normal';
			}

			$itemDrops[$dropField]['chance'] = $itemDrops[$dropField]['chance'] * $adjust / 10000;

			if ($itemDrops[$dropField]['chance'] > 100) {
				$itemDrops[$dropField]['chance'] = 100;
			}
		}
	}

	$sql = "SELECT * FROM $skillDB WHERE mob_id = ?";
	$sth = $server->connection->getStatement($sql);
	$sth->execute(array($mobID));
	$mobSkills = $sth->fetchAll();

//	echo "<pre>";
//	print_r($monster);
//	print_r($itemDrops);
//	echo "</pre>";

	/**
	 * Custom API
	 */
	// Size
	$size = $monster->size;
	switch ($size) {
		case 0:
			$size = "Nhỏ (Small)"; break;
		case 1;
			$size = "Vừa (Medium)"; break;
		case 2:
			$size = "Lớn (Large)"; break;
	}

	// Race
	$race = $monster->race;
	switch ($race) {
		case 0:
			$race = "Formless"; break;
		case 1:
			$race = "Thây ma (Undead)"; break;
		case 2:
			$race = "Quái thú (Brute)"; break;
		case 3:
			$race = "Thực vật (Plant)"; break;
		case 4:
			$race = "Côn trùng (Insect)"; break;
		case 5:
			$race = "Cá (Fish)"; break;
		case 6:
			$race = "Quỷ (Demon)"; break;
		case 7:
			$race = "Á thần (Demi-human)"; break;
		case 8:
			$race = "Thiên thần (Angel)"; break;
		case 9:
			$race = "Rồng (Dragon)"; break;
	}

	// Element
	$element = (int)$monster->element_type;
	$element_lv = floor($monster->element_level);
	switch ($element) {
		case 0:
			$element = "Trung tính ". $element_lv ." (Neutral ". $element_lv .")"; break;
		case 1:
			$element = "Nước ". $element_lv ." (Water ". $element_lv .")"; break;
		case 2:
			$element = "Đất ". $element_lv ." (Earth ". $element_lv .")"; break;
		case 3:
			$element = "Lửa ". $element_lv ." (Fire ". $element_lv .")"; break;
		case 4;
			$element = "Gió ". $element_lv ." (Wind ". $element_lv .")"; break;
		case 5:
			$element = "Độc ". $element_lv ." (Poison ". $element_lv .")"; break;
		case 6:
			$element = "Thánh ". $element_lv ." (Holy ". $element_lv .")"; break;
		case 7:
			$element = "Bóng tối ". $element_lv ." (Shadow ". $element_lv .")"; break;
		case 8:
			$element = "Thây ma ". $element_lv ." (Undead ". $element_lv .")"; break;
	}

	// Calculate ATK, MATK
	$level = $monster->level;
	$str = (int)$monster->strength;
	$int = (int)$monster->intelligence;
	$atk = (int)$monster->attack1;
	$matk = (int)$monster->attack2;

	$minAtk = ($atk*80/100) + $level + $str;
	$maxAtk = ($atk*120/100) + $level + $str;

	$minMatk = ($matk*70/100) + $level + $int;
	$maxMatk = ($matk*130/100) + $level + $int;

	// Create array
	$monsterInfo = array(
		'mobInfo' => array(
			'id' => (int)$monster->monster_id,
			'level' => (int)$level,
			'hp' => (int)$monster->hp,
			'name' => $monster->kro_name,
			'type' => $monster_type = get_monster_type($monster),
			'baseExp' => $monster->base_exp,
			'jobExp' => $monster->job_exp,
			'mvpExp' => $monster->mvp_exp,
			'def' => (int)$monster->defense,
			'mdef' => (int)$monster->magic_defense,
			'minAtk' => floor($minAtk),
			'maxAtk' => floor($maxAtk),
			'minMatk' => floor($minMatk),
			'maxMatk' => floor($maxMatk),
			'str' => $str,
			'agi' => (int)$monster->agility,
			'vit' => (int)$monster->vitality,
			'int' => $int,
			'dex' => (int)$monster->dexterity,
			'luk' => (int)$monster->luck,
			'size' => $size,
			'race' => $race,
			'element' => $element,
			'elementLv' => $element_lv,
			'speed' => (int)$monster->speed,
			'drops' => $itemDrops
		)
	);

	echo json_encode($monsterInfo, JSON_UNESCAPED_UNICODE);
	die;
}
?>
